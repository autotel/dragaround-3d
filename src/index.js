import * as THREE from 'three';
import Dragaround from "./Dragaround";
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls' 

let dom_board=document.createElement("div");
dom_board.id="drag-board";
document.body.appendChild(dom_board);
const dragBoard=new Dragaround(dom_board);

 
var camera, scene, renderer;
var geometry, material, mesh;
var raycaster,control;
const frameRedrawList=new Set();
const Thing=function(){}


/** 
 * get the scene coordinates projected from a screen position and a distance from camera
 * useful for dragging objects around
 * z is distance from camera
 */
const projectionSphere = new THREE.SphereGeometry( 4 , 12 , 8 );//new THREE.SphereGeometry( 10, 12, 8 );

var wireMaterial = new THREE.MeshBasicMaterial( {
	color: 0xffffff,
	linewidth: 0.1,
    wireframe:true,
    side:THREE.BackSide,
} );

var projectionSphereMesh = new THREE.Mesh( projectionSphere, wireMaterial );
setTimeout(()=>{
    scene.add( projectionSphereMesh );
},100);

function screenToScene({x,y,_z}){
    const normCoord={
        x:   ( x / window.innerWidth ) * 2 - 1,
        y: - ( y / window.innerHeight ) * 2 + 1
    }

    raycaster.setFromCamera( normCoord, camera );
    // console.log(raycaster.intersectObject( projectionSphereMesh ));
	// calculate objects intersecting the picking ray
	return raycaster.intersectObject( projectionSphereMesh )[0].point;
}
/** untested, get the list of objects that fall under a certain point in the screen */
function screenToObjects({x,y}){
    const normCoord={
        x:   ( x / window.innerWidth ) * 2 - 1,
        y: - ( y / window.innerHeight ) * 2 + 1
    }
    raycaster.setFromCamera( normCoord, camera );

	// calculate objects intersecting the picking ray
	return raycaster.intersectObjects( scene.children );
}

/**
 * get the screen position of a scene coordinate 
 * z is distance from camera
 * @returns {{x:number, y:number,z:number}}
 */
function sceneToScreen({x,y,z}){
    const width = window.innerWidth;
    const height = window.innerHeight;
    var widthHalf = width / 2, heightHalf = height / 2;

    var vector = new THREE.Vector3(x,y,z);
    vector.project(camera );

    vector.x = ( vector.x * widthHalf ) + widthHalf;
    vector.y = - ( vector.y * heightHalf ) + heightHalf;
    vector.z = vector.distanceTo(camera.position);

    return vector;
}

// a thing that has a visible shape
const Sprite=function(){
    this.frame=function(){}
    this.sprite = new THREE.Group();
    scene.add(this.sprite);
    frameRedrawList.add(this);
    //should have a group where all the meshes and stuff is appended
}
// a thing that can be dragged
const Draggable=function(){
    this.beingDragged=false;

    this.udpateHandlePosition=()=>{
        if(this.sprite){
            const spritePosition=this.sprite.position;
            const gottenPos=sceneToScreen(spritePosition);
            // console.log(gottenPos);
            this.handle.reposition(gottenPos);
        }else{
            console.warn("no sprite");
        }
    }

    this.handle=dragBoard.createHandle({
        x:0,
        y:0,
        z:1,
    });

    this.handle.onDrag=()=>{
        if(this.sprite){

            const scenePosition=screenToScene(this.handle.position);
            // console.log(scenePosition);
            Object.assign(
                this.sprite.position,
                scenePosition,
            );
        }else{
            console.warn("no sprite");
        }
    }

    this.handle.onDragStart=()=>{
        this.beingDragged=true;
    }
    
    this.handle.onDragEnd=()=>{
        this.beingDragged=false;
    }
}
//a cube that can be dragged around
const TestThing=function(){
    Thing.call(this);
    Sprite.call(this);
    Draggable.call(this);

    geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
    material = new THREE.MeshNormalMaterial();
 
    mesh = new THREE.Mesh( geometry, material );

    this.sprite.add( mesh );
    
    this.sprite.position.y = 0.4;
    this.frame=()=>{
        this.sprite.rotation.x += 0.01;
        this.sprite.rotation.y += 0.02;
        if(!this.beingDragged)
            this.udpateHandlePosition();
    }

}

let testThings;

init();
animate();
 
function init() {
    renderer = new THREE.WebGLRenderer( { antialias: true } );

    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10 );
    camera.position.z = 1;
 
    scene = new THREE.Scene();
    
    control = new OrbitControls( camera, dom_board );
    raycaster = new THREE.Raycaster();

    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    testThings=[
        new TestThing(),
        new TestThing(),
        new TestThing(),
        new TestThing(),
    ];
}
 
function animate() {
 
    requestAnimationFrame( animate );
 
 
    renderer.render( scene, camera );

    frameRedrawList.forEach((a)=>a.frame());

    control.update();
 
}

